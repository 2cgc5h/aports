# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qqc2-desktop-style
pkgver=5.91.0
pkgrel=0
pkgdesc="A style for Qt Quick Controls 2 to make it follow your desktop theme"
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-only AND (GPL-2.0-only OR GPL-3.0-only)"
depends="
	qt5-qtgraphicaleffects
	qt5-qtquickcontrols2
	"
depends_dev="
	kconfigwidgets-dev
	kiconthemes-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/qqc2-desktop-style-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
f8a63adc6ee2a8246ce44fd762949f7be3a1853a192576acf2ecbee7640103e7668b1fbd31729076269ed67f6ab56855318794a66b2448a420381e47a52bc569  qqc2-desktop-style-5.91.0.tar.xz
"
