# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=dasel
pkgver=1.23.0
pkgrel=0
pkgdesc="Query and modify data structures using selector strings"
url="https://daseldocs.tomwright.me/"
license="MIT"
arch="all"
makedepends="go"
source="https://github.com/TomWright/dasel/archive/v$pkgver/dasel-$pkgver.tar.gz"

export GOPATH="$srcdir"
export GOFLAGS="$GOFLAGS -trimpath -modcacherw"
export CGO_ENABLED=0

build() {
	go build \
		-ldflags "-X github.com/tomwright/dasel/internal.Version=$pkgver" \
		-v \
		./cmd/dasel
}

check() {
	go test ./...
}

package() {
	install -Dm755 dasel "$pkgdir"/usr/bin/dasel
}

sha512sums="
f4383ddd99b6b7379a7d6feae929ac691abc1ecfc154f281f9f205300799803129258a00856ebecfefca3c3833ebdd4a301656486faa014096f16eb400972da1  dasel-1.23.0.tar.gz
"
