# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=ansible-lint
pkgver=5.4.0
pkgrel=0
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	python3
	ansible-core
	py3-enrich
	py3-packaging
	py3-rich
	py3-ruamel.yaml
	py3-tenacity
	py3-tomli
	py3-typing-extensions
	py3-wcmatch
	py3-yaml
	"
makedepends="
	py3-pip
	py3-build
	py3-setuptools
	py3-wheel
	py3-setuptools_scm
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
source="ansible-lint-5.4.0.tar.gz::https://github.com/ansible-community/ansible-lint/archive/refs/tags/v$pkgver.tar.gz"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	python3 -m build --skip-dependency-check --no-isolation --wheel .
}

check() {
	pytest
}

package() {
	pip3 install --no-warn-script-location --ignore-installed --no-deps --root="$pkgdir" dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
f1bbbb49eb77991d60b6087646c695a938d328d719bba67707af4ed858de857d383b78490bcf0930cffebd13a0b0abde20269a47e47e4713a37da715a28c5da0  ansible-lint-5.4.0.tar.gz
"
