# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktexteditor
pkgver=5.91.0
pkgrel=0
pkgdesc="Advanced embeddable text editor"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.0-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kauth-dev
	kconfig-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	sonnet-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
options="!check" # fail on builders
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktexteditor-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
92438aaabfc82f1b18935636caefe59d3a7624ada8f276f43c61efb2b96fb9df560f38c175dd35d95db70b1a931d6567a32b71bbb798de75a17c4c65e1809d9d  ktexteditor-5.91.0.tar.xz
"
